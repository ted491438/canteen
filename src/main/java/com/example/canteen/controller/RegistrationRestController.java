package com.example.canteen.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.canteen.dao.RoleRepository;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.domain.Role;
import com.example.canteen.domain.User;
import com.example.canteen.dto.UserDto;
import com.example.canteen.event.OnRegistrationCompleteEvent;
import com.example.canteen.service.UserService;
import com.example.canteen.util.GenericResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationRestController {

  private final UserService userService;
  private final ApplicationEventPublisher eventPublisher;
  
  @PostMapping(value = "/user/registration")
  public GenericResponse registerUserAccount(@Valid UserDto accountDto, final HttpServletRequest request) {
    log.info("Registering user account with information: {}", accountDto);
    
    final User registered = userService.registerNewUserAccount(accountDto);
    eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), getAppUrl(request)));
    
    return new GenericResponse("success");
  }
  
  private String getAppUrl(HttpServletRequest request) {
    
    return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
  }
}
