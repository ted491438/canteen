package com.example.canteen.controller;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.domain.Privilege;
import com.example.canteen.domain.Role;
import com.example.canteen.domain.User;
import com.example.canteen.domain.VerificationToken;
import com.example.canteen.dto.UserDto;
import com.example.canteen.service.SecurityUserService;
import com.example.canteen.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegisterationController {
  
  private final MessageSource messages;
  private final UserService userService;
  private final JavaMailSender mailSender;
  private final Environment env;

  @GetMapping("/user/registration")
  public String showRegistrationForm(final HttpServletRequest request, final Model model) {
    log.debug("Rendering registration page.");
    final UserDto accountDto = new UserDto();
    model.addAttribute("user", accountDto);
    
    return "registration";
  }

  @GetMapping("/user/resendRegistrationToken")
  public String resendRegistrationToken(final HttpServletRequest request, final Model model, @RequestParam("token") final String existingToken) {
    final Locale locale = request.getLocale();
    final VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
    final User user = userService.getUser(newToken.getToken());
    try {
      final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
      final SimpleMailMessage email = constructResetVerificationTokenEmail(appUrl, request.getLocale(), newToken, user);
      mailSender.send(email);
    } catch (final MailAuthenticationException e) {
      log.debug("MailAuthenticationException", e);
      return "redirect:/emailError.html?lang=" + locale.getLanguage();
    } catch (final Exception e) {
      log.debug(e.getLocalizedMessage(), e);
      model.addAttribute("message", e.getLocalizedMessage());
      
      return "redirect:/login.html?lang=" + locale.getLanguage();
    }
    model.addAttribute("message", messages.getMessage("message.resendToken", null, locale));
    
    return "redirect:/login.html?lang=" + locale.getLanguage();
  }

  @GetMapping("/login")
  public ModelAndView login(final HttpServletRequest request, final ModelMap model, @RequestParam("messageKey" ) final Optional<String> messageKey, @RequestParam("message" ) final Optional<String> message, @RequestParam("error" ) final Optional<String> error) {
    log.info("messageKey: {}", messageKey);
    Locale locale = request.getLocale();
    model.addAttribute("lang", locale.getLanguage());
    messageKey.ifPresent(key -> {
      model.addAttribute("message", messages.getMessage(key, null, locale));
    });
    message.ifPresent(key -> {
      model.addAttribute("message", message.get());
    });
    
    error.ifPresent(e -> model.addAttribute("error", e));

    return new ModelAndView("login", model);
  }

  @GetMapping("/registrationConfirm")
  public ModelAndView confirmRegistration(final HttpServletRequest request, final ModelMap model, @RequestParam("token") final String token) throws UnsupportedEncodingException {
    Locale locale = request.getLocale();
    model.addAttribute("lang", locale.getLanguage());
    final String result = userService.validateVerificationToken(token);
    if (result.equals("valid")) {
      final User user = userService.getUser(token);
      authWithoutPassword(user);
      model.addAttribute("messageKey", "message.accountVerified");

      return new ModelAndView("/successRegister", model);
    }
    model.addAttribute("messageKey", "auth.message." + result);
    model.addAttribute("expired", "expired".equals(result));
    model.addAttribute("token", token);

    return new ModelAndView("badUser", model);
  }

  public void authWithoutPassword(User user) {
    List<Privilege> privileges = user.getRoles()
        .stream()
        .map(Role::getPrivileges)
        .flatMap(Collection::stream)
        .distinct()
        .collect(Collectors.toList());

    List<GrantedAuthority> authorities = privileges.stream()
        .map(p -> new SimpleGrantedAuthority(p.getName()))
        .collect(Collectors.toList());

    Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);
    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  private SimpleMailMessage constructResetVerificationTokenEmail(final String contextPath, final Locale locale, final VerificationToken newToken, final User user) {
    final String confirmationUrl = contextPath + "/registrationConfirm?token=" + newToken.getToken();
    final String message = messages.getMessage("message.resendToken", null, locale);
    final SimpleMailMessage email = new SimpleMailMessage();
    email.setSubject("Resend Registration Token");
    email.setText(message + " \r\n" + confirmationUrl);
    email.setTo(user.getEmail());
    email.setFrom(env.getProperty("support.email"));
    
    return email;
  }
}