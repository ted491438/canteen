package com.example.canteen.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.canteen.domain.FoodOrderItem;
import com.example.canteen.domain.pk.FoodOrderItemId;

public interface FoodOrderItemRepository extends JpaRepository<FoodOrderItem, FoodOrderItemId>{

}
