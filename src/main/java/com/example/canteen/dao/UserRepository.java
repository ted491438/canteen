package com.example.canteen.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.canteen.domain.User;

public interface UserRepository extends DataTablesRepository<User, Long> {
  
  User findByEmail(String email);

  @Override
  void delete(User user);

}
