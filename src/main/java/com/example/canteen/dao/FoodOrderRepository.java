package com.example.canteen.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import com.example.canteen.domain.FoodOrder;

public interface FoodOrderRepository extends DataTablesRepository<FoodOrder, Long> {

}
