package com.example.canteen.dao;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.canteen.domain.Role;

public interface RoleRepository extends DataTablesRepository<Role, Long> {

  Role findByName(String name);

  @Override
  void delete(Role role);

}
