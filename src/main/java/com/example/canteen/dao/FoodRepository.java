package com.example.canteen.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.canteen.domain.Food;

public interface FoodRepository extends JpaRepository<Food, Long> {

  Food findByName(String name);

}
