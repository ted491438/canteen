package com.example.canteen.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.domain.Privilege;
import com.example.canteen.domain.Role;
import com.example.canteen.domain.User;
import com.example.canteen.service.MyUserDetailService;
import com.example.canteen.service.UserService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MyUserDetailsService implements MyUserDetailService {

  private final UserRepository userRepository;
  private final HttpServletRequest request;

  @Override
  public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
    //final String ip = getClientIP();

    try {
      final User user = userRepository.findByEmail(email);
      if (user == null) {

        throw new UsernameNotFoundException("No user found with username: " + email);
      }

      return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), user.isEnabled(), true, true, true, getAuthorities(user.getRoles()));
    } catch (final Exception e) {

      throw new RuntimeException(e);
    }
  }

  private Collection<? extends GrantedAuthority> getAuthorities(final Collection<Role> roles) {

    return getGrantedAuthorities(getPrivileges(roles));
  }

  private List<String> getPrivileges(final Collection<Role> roles) {
    final List<String> privileges = new ArrayList<>();
    final List<Privilege> collection = new ArrayList<>();
    for (final Role role : roles) {
      collection.addAll(role.getPrivileges());
    }
    for (final Privilege item : collection) {
      privileges.add(item.getName());
    }

    return privileges;
  }

  private List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
    final List<GrantedAuthority> authorities = new ArrayList<>();
    for (final String privilege : privileges) {
      authorities.add(new SimpleGrantedAuthority(privilege));
    }

    return authorities;
  }

  private String getClientIP() {
    final String xfHeader = request.getHeader("X-Forwarded-For");
    if (xfHeader != null) {
      return xfHeader.split(",")[0];
    }

    return request.getRemoteAddr();
  }

}