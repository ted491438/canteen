package com.example.canteen.service.impl;

import java.util.Calendar;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.canteen.dao.PasswordResetTokenRepository;
import com.example.canteen.domain.PasswordResetToken;
import com.example.canteen.service.SecurityUserService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SecurityUserServiceImpl implements SecurityUserService {

  private final PasswordResetTokenRepository passwordTokenRepository;

  @Override
  public String validatePasswordResetToken(String token) {
    final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);

    return !isTokenFound(passToken) ? "invalidToken"
        : isTokenExpired(passToken) ? "expired"
            : null;
  }

  private boolean isTokenFound(PasswordResetToken passToken) {

    return passToken != null;
  }

  private boolean isTokenExpired(PasswordResetToken passToken) {
    final Calendar cal = Calendar.getInstance();

    return passToken.getExpiryDate().before(cal.getTime());
  }
}
