package com.example.canteen.service.impl;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import com.example.canteen.dao.FoodOrderRepository;
import com.example.canteen.domain.FoodOrder;
import com.example.canteen.service.FoodOrderService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FoodOrderServiceImpl implements FoodOrderService {

  private final FoodOrderRepository foodOrderRepository;
  
  public DataTablesOutput<FoodOrder> findAllByDataTablesInput(DataTablesInput input) {
    
    return foodOrderRepository.findAll(input);
  }
  
}
