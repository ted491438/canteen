package com.example.canteen.service.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.canteen.controller.RegistrationRestController;
import com.example.canteen.dao.PasswordResetTokenRepository;
import com.example.canteen.dao.RoleRepository;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.dao.VerificationTokenRepository;
import com.example.canteen.domain.PasswordResetToken;
import com.example.canteen.domain.User;
import com.example.canteen.domain.VerificationToken;
import com.example.canteen.dto.UserDto;
import com.example.canteen.exception.UserAlreadyExistException;
import com.example.canteen.helper.SecurityHelper;
import com.example.canteen.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

  public static final String TOKEN_INVALID = "invalidToken";
  public static final String TOKEN_EXPIRED = "expired";
  public static final String TOKEN_VALID = "valid";

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final PasswordResetTokenRepository passwordTokenRepository;
  private final RoleRepository roleRepository;
  private final VerificationTokenRepository tokenRepository;
  private final SecurityHelper securityHelper;

  public User registerNewUserAccount(final UserDto accountDto) {
    if (emailExists(accountDto.getEmail())) {
      throw new UserAlreadyExistException("There is an account with that email address: " + accountDto.getEmail());
    }
    final User user = new User();

    user.setFirstName(accountDto.getFirstName());
    user.setLastName(accountDto.getLastName());
    user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
    user.setEmail(accountDto.getEmail());
    user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
    
    return userRepository.save(user);
  }

  @Override
  public void createPasswordResetTokenForUser(final User user, final String token) {
    final PasswordResetToken myToken = new PasswordResetToken(token, user, securityHelper.calculateExpiryDate(PasswordResetToken.EXPIRATION));
    passwordTokenRepository.save(myToken);
  }


  @Override
  public void createVerificationTokenForUser(final User user, final String token) {
    final VerificationToken myToken = new VerificationToken(token, user, securityHelper.calculateExpiryDate(VerificationToken.EXPIRATION));
    tokenRepository.save(myToken);
  }

  @Override
  public String validateVerificationToken(String token) {
    final VerificationToken verificationToken = tokenRepository.findByToken(token);
    if (verificationToken == null) {
      
      return TOKEN_INVALID;
    }

    final User user = verificationToken.getUser();
    final Calendar cal = Calendar.getInstance();
    if ((verificationToken.getExpiryDate()
        .getTime() - cal.getTime()
        .getTime()) <= 0) {
//      tokenRepository.delete(verificationToken);
      
      return TOKEN_EXPIRED;
    }

    user.setEnabled(true);
    // tokenRepository.delete(verificationToken);
    userRepository.save(user);
    
    return TOKEN_VALID;
  }

  @Override
  public User findUserByEmail(final String email) {
    
    return userRepository.findByEmail(email);
  }

  @Override
  public PasswordResetToken getPasswordResetToken(final String token) {
    
    return passwordTokenRepository.findByToken(token);
  }

  @Override
  public Optional<User> getUserByPasswordResetToken(final String token) {
    
    return Optional.ofNullable(passwordTokenRepository.findByToken(token) .getUser());
  }
  
  @Override
  public void changeUserPassword(final User user, final String password) {
    user.setPassword(passwordEncoder.encode(password));
    userRepository.save(user);
  }

  private boolean emailExists(final String email) {
    
    return userRepository.findByEmail(email) != null;
  }

  @Override
  public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
    
    return passwordEncoder.matches(oldPassword, user.getPassword());
  }
  
  @Override
  public User getUser(final String verificationToken) {
      final VerificationToken token = tokenRepository.findByToken(verificationToken);
      if (token != null) {
          return token.getUser();
      }
      
      return null;
  } 
  
  @Override
  public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
      VerificationToken vToken = tokenRepository.findByToken(existingVerificationToken);
      vToken.setToken(UUID.randomUUID().toString());
      vToken.setExpiryDate(securityHelper.calculateExpiryDate(VerificationToken.EXPIRATION));
      vToken = tokenRepository.save(vToken);
      
      return vToken;
  }
  
}
