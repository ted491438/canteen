package com.example.canteen.service;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import com.example.canteen.domain.FoodOrder;

public interface FoodOrderService {
  
  public DataTablesOutput<FoodOrder> findAllByDataTablesInput(DataTablesInput input);

}
