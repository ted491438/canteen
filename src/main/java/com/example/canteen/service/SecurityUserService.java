package com.example.canteen.service;

public interface SecurityUserService {
  
  String validatePasswordResetToken(String token);
  
}
