package com.example.canteen.service;

import java.util.Optional;
import com.example.canteen.domain.PasswordResetToken;
import com.example.canteen.domain.User;
import com.example.canteen.domain.VerificationToken;
import com.example.canteen.dto.UserDto;
import com.example.canteen.exception.UserAlreadyExistException;

public interface UserService {

  User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;
  
  void createVerificationTokenForUser(User user, String token);
  
  User findUserByEmail(String email);
  
  PasswordResetToken getPasswordResetToken(String token);
  
  void createPasswordResetTokenForUser(User user, String token);
  
  Optional<User> getUserByPasswordResetToken(String token);
  
  void changeUserPassword(User user, String password);
  
  boolean checkIfValidOldPassword(User user, String oldPassword);
  
  String validateVerificationToken(String token);
  
  User getUser(String verificationToken);
  
  VerificationToken generateNewVerificationToken(String existingVerificationToken);
  
}
