package com.example.canteen.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface MyUserDetailService extends UserDetailsService {

}
