package com.example.canteen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import com.example.canteen.handle.CustomAuthenticationFailureHandler;
import com.example.canteen.service.UserService;
import com.example.canteen.service.impl.MyUserDetailsService;
import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final MyUserDetailsService userDetailsService;
  private final CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder(11);
  }
  
  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
//    .csrf().disable()
    .authorizeRequests()
    .antMatchers("/login", "/registrationConfirm*", "/user/resendRegistrationToken*", "/user/registration", "/successRegister", "/css/**", "/assets/**", "/js/**", "/image/**").permitAll()
    .anyRequest().authenticated()
    .and()
    .formLogin()
    .loginPage("/login")
//    .defaultSuccessUrl("/", true)
    .failureHandler(customAuthenticationFailureHandler)
    .and()
    .logout();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //auth.userDetailsService(userDetailsService);
    auth.authenticationProvider(authProvider());
  }

  @Bean
  public DaoAuthenticationProvider authProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(encoder());
    
    return authProvider;
  }

}

