package com.example.canteen.domain;

import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Food {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  @Column(length = 50, nullable = false)
  private String name;
  
  @Column(columnDefinition = "TINYINT", nullable = false)
  private Category category; 
  
  public enum Category {
    CHINESECUISINE,
    JAPANESECUISINE, 
    SNACK;
  }
  
  @Column(length = 500)
  private String description;
  
  @Column(precision = 12, scale = 2, nullable = false)
  private BigDecimal price;
  
  @OneToMany(mappedBy = "food")
  private Collection<FoodOrderItem> foodOrderItems;

}
