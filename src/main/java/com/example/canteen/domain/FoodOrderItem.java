package com.example.canteen.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import com.example.canteen.domain.pk.FoodOrderItemId;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@IdClass(FoodOrderItemId.class)
public class FoodOrderItem {
  
  @Id
  @ManyToOne
  @JsonManagedReference
  @JoinColumn(name = "food_order_id", nullable = false)
  private FoodOrder foodOrder;
  
  @Id
  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "food_id", nullable = false)
  private Food food;
  
  private Short quantity;
  
  private Date updatedAt;
  
  public FoodOrderItem(FoodOrder foodOrder, Food food) {
    this.foodOrder = foodOrder;
    this.food = food;
  }
  
  @PrePersist
  public void PrePersist() {
    updatedAt = new Date();
  } 
  
  @PreUpdate
  public void preUpdate() {
    updatedAt = new Date();
  } 
  
}
