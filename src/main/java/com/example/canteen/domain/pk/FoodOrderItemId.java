package com.example.canteen.domain.pk;

import java.io.Serializable;
import com.example.canteen.domain.Food;
import com.example.canteen.domain.FoodOrder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
public class FoodOrderItemId implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long foodOrder;
  
  private Long food;
  
  public FoodOrderItemId() {
    super();
  }

  public FoodOrderItemId(Long foodOrder, Long food) {
    super();
    this.foodOrder = foodOrder;
    this.food = food;
  }
  
}
