package com.example.canteen.domain;

import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
public class FoodOrder {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  private String mobilePhone;
  
  private String address;
  
  @Column(columnDefinition = "TINYINT")
  private DeliveryType deliveryType;
  
  public enum DeliveryType {
    DELIVERY,
    PICKUP
  }
  
  @ManyToOne
  @JsonManagedReference
  @JoinColumn(name = "user_account_id", nullable = false)
  private User user;
  
  @OneToMany(mappedBy = "foodOrder")
  @ToString.Exclude
  @JsonBackReference
  private Collection<FoodOrderItem> foodOrderItems;
  
  private Date createdAt;
  
  @PrePersist
  public void prePersist() {
    createdAt = new Date();
  }
  
   
}
