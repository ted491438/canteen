package com.example.canteen.listener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.example.canteen.dao.FoodOrderItemRepository;
import com.example.canteen.dao.FoodOrderRepository;
import com.example.canteen.dao.FoodRepository;
import com.example.canteen.dao.PrivilegeRepository;
import com.example.canteen.dao.RoleRepository;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.domain.Food;
import com.example.canteen.domain.FoodOrder;
import com.example.canteen.domain.FoodOrderItem;
import com.example.canteen.domain.Privilege;
import com.example.canteen.domain.Role;
import com.example.canteen.domain.User;
import com.example.canteen.domain.pk.FoodOrderItemId;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

  private boolean alreadySetup = false;
  
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final PrivilegeRepository privilegeRepository;
  private final PasswordEncoder passwordEncoder;
  private final FoodRepository foodRepository;
  private final FoodOrderRepository foodOrderRepository;
  private final FoodOrderItemRepository foodOrderItemRepository;

  @Override
  @Transactional
  public void onApplicationEvent(final ContextRefreshedEvent event) {
    if (alreadySetup) {
      return;
    }

    // create initial privileges
    final Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
    final Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
    final Privilege passwordPrivilege = createPrivilegeIfNotFound("CHANGE_PASSWORD_PRIVILEGE");

    // create initial roles
    final List<Privilege> adminPrivileges = new ArrayList<>(Arrays.asList(readPrivilege, writePrivilege, passwordPrivilege));
    final List<Privilege> userPrivileges = new ArrayList<>(Arrays.asList(readPrivilege, passwordPrivilege));
    final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
    createRoleIfNotFound("ROLE_USER", userPrivileges);

    // create initial user
    createUserIfNotFound("test@test.com", "Test", "Test", "test", new ArrayList<>(Arrays.asList(adminRole)));

    // create initial orders for demonstration
    createFoodOrderItem(
        createFoodIfNotFound("Dim Sums", Food.Category.CHINESECUISINE, BigDecimal.valueOf(5.45), "An assortment of dumplings made with chicken and prawns."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("Hot and Sour Soup", Food.Category.CHINESECUISINE, BigDecimal.valueOf(3.45), "A piping hot bowl of hot and sour soup on a wintery evening is all you need. Mushrooms, cabbage and carrots cooked in oriental flavors with chicken."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("Sushi1", Food.Category.CHINESECUISINE, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("Sushi2", Food.Category.JAPANESECUISINE, BigDecimal.valueOf(5.45), "An assortment of dumplings made with chicken and prawns."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("Sushi3", Food.Category.JAPANESECUISINE, BigDecimal.valueOf(3.45), "A piping hot bowl of hot and sour soup on a wintery evening is all you need. Mushrooms, cabbage and carrots cooked in oriental flavors with chicken."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("Sushi4", Food.Category.JAPANESECUISINE, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("cookies1", Food.Category.SNACK, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("cookies2", Food.Category.SNACK, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.PICKUP), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("cake1", Food.Category.SNACK, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("cake2", Food.Category.SNACK, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.PICKUP), 
        (short)2);
    createFoodOrderItem(
        createFoodIfNotFound("cake3", Food.Category.SNACK, BigDecimal.valueOf(1.45), "Noodles tossed with veggies, lemon juice, peanuts and seasoned to perfection. Kids would love this noodles recipe."), 
        createFoodOrder("test@test.com", "0900111111", "Taiwan city", FoodOrder.DeliveryType.DELIVERY), 
        (short)2);
    
    
    
    
    alreadySetup = true;
  }
  
  @Transactional
  FoodOrderItem createFoodOrderItem(final Food food, final FoodOrder foodOrder, short quantity) {
    FoodOrderItem foodOrderItem = 
        foodOrderItemRepository
          .findById(new FoodOrderItemId(foodOrder.getId(), food.getId()))
          .orElseGet(() -> new FoodOrderItem(foodOrder, food));
    foodOrderItem.setQuantity(quantity);
    
    return foodOrderItemRepository.save(foodOrderItem);
  }
  
  @Transactional
  FoodOrder createFoodOrder(final String email, final String mobilePhone, final String address, final FoodOrder.DeliveryType deliveryType) {
    User user = userRepository.findByEmail(email);
    FoodOrder foodOrder = null;
    if (user != null) {
      foodOrder = new FoodOrder();
      foodOrder.setUser(user);
      foodOrder.setMobilePhone(mobilePhone);
      foodOrder.setAddress(address);
      foodOrder.setDeliveryType(deliveryType);
      foodOrderRepository.save(foodOrder);
    } 
    
    return foodOrder;
  }
  
  @Transactional
  Food createFoodIfNotFound(final String name, final Food.Category category, final BigDecimal price, final String description) {
    Food food = foodRepository.findByName(name);
    if (food == null) {
      food = new Food();
      food.setName(name);
      food.setCategory(category);
      food.setDescription(description);
      food.setPrice(price);
    }
    foodRepository.save(food);
   
    return food;
  }

  @Transactional
  Privilege createPrivilegeIfNotFound(final String name) {
    Privilege privilege = privilegeRepository.findByName(name);
    if (privilege == null) {
      privilege = new Privilege(name);
      privilege = privilegeRepository.save(privilege);
    }
    
    return privilege;
  }

  @Transactional
  Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
    Role role = roleRepository.findByName(name);
    if (role == null) {
      role = new Role(name);
    }
    role.setPrivileges(privileges);
    role = roleRepository.save(role);
    return role;
  }

  @Transactional
  User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final Collection<Role> roles) {
    User user = userRepository.findByEmail(email);
    if (user == null) {
      user = new User();
      user.setFirstName(firstName);
      user.setLastName(lastName);
      user.setPassword(passwordEncoder.encode(password));
      user.setEmail(email);
      user.setEnabled(true);
    }
    user.setRoles(roles);
    user = userRepository.save(user);
    return user;
  }

}
