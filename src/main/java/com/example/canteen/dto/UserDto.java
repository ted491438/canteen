package com.example.canteen.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.example.canteen.validation.PasswordMatches;
import com.example.canteen.validation.ValidEmail;
import com.example.canteen.validation.ValidPassword;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@PasswordMatches
@Data
public class UserDto {
  
    @NotNull
    @NotBlank
    @Size(min = 1, message = "{Size.userDto.firstName}")
    private String firstName;

    @NotNull
    @Size(min = 1, message = "{Size.userDto.lastName}")
    private String lastName;
    
    @ValidPassword
    @NotNull
    @NotBlank
    private String password;

    @NotNull
    @Size(min = 1)
    private String matchingPassword;

    @Email
    @NotNull
    @Size(min = 1, message = "{Size.userDto.email}")
    private String email;

    private Integer role;

}