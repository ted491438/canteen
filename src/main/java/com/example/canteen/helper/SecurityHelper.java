package com.example.canteen.helper;

import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class SecurityHelper {
  
  public Date calculateExpiryDate(final int expiryTimeInMinutes) {
    final Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(new Date().getTime());
    cal.add(Calendar.MINUTE, expiryTimeInMinutes);
    return new Date(cal.getTime().getTime());
  } 
  
}
