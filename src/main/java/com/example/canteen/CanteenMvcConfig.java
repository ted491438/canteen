package com.example.canteen;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.validation.EmailValidator;
import com.example.canteen.validation.PasswordMatchesValidator;
import lombok.RequiredArgsConstructor;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CanteenMvcConfig implements WebMvcConfigurer {
  
  private final MessageSource messageSource;

  @Override
  public void addViewControllers(final ViewControllerRegistry registry) {
    registry.addViewController("/successRegister").setViewName("successRegister");
//    registry.addViewController("/").setViewName("forward:/login");
//    registry.addViewController("/login").setViewName("login");
//    registry.addViewController("/loginRememberMe");
//    registry.addViewController("/customLogin");
//    registry.addViewController("/registration.html");
//    registry.addViewController("/registrationCaptcha.html");
//    registry.addViewController("/registrationReCaptchaV3.html");
//    registry.addViewController("/logout.html");
//    registry.addViewController("/homepage.html");
//    registry.addViewController("/expiredAccount.html");
//    registry.addViewController("/emailError.html");
//    registry.addViewController("/home.html");
//    registry.addViewController("/invalidSession.html");
//    registry.addViewController("/admin.html");
//    registry.addViewController("/successRegister.html");
//    registry.addViewController("/forgetPassword.html");
//    registry.addViewController("/updatePassword.html");
//    registry.addViewController("/changePassword.html");
//    registry.addViewController("/users.html");
//    registry.addViewController("/qrcode.html");
  }
  
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
      registry
        .addResourceHandler("/resources/**")
        .addResourceLocations("/resources/"); 
  }
  
  @Bean
  public EmailValidator usernameValidator() {
      return new EmailValidator();
  }

}
