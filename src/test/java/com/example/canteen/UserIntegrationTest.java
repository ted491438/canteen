package com.example.canteen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.WebApplicationContext;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.dao.VerificationTokenRepository;
import com.example.canteen.domain.User;
import com.example.canteen.domain.VerificationToken;
import com.example.canteen.dto.UserDto;
import com.example.canteen.helper.SecurityHelper;
import com.example.canteen.service.UserService;
import lombok.RequiredArgsConstructor;

@SpringBootTest
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserIntegrationTest {

  private final VerificationTokenRepository tokenRepository;
  private final UserRepository userRepository;
  private final SecurityHelper securityHelper;
  private final PasswordEncoder passwordEncoder;

  @PersistenceContext
  private EntityManager entityManager;

  private Long tokenId;
  private String tokenChars;
  private Long userId;
  private String password;

  @BeforeEach
  public void setUp() throws Exception {
    User user = new User();
    user.setEmail("youraccount@test.com");
    password = String.valueOf((int)(Math.random() * 999999));
    user.setPassword(passwordEncoder.encode(password));
    user.setFirstName("Your First Name");
    user.setLastName("Your Lasst Name");
    entityManager.persist(user);

    String token = UUID.randomUUID().toString();
    VerificationToken verificationToken = new VerificationToken(token, user, securityHelper.calculateExpiryDate(VerificationToken.EXPIRATION));
    entityManager.persist(verificationToken);

    entityManager.flush();
    entityManager.clear();

    tokenId = verificationToken.getId();
    tokenChars = verificationToken.getToken();
    userId = user.getId();
    System.out.println(user.getPassword());
    System.out.println(userId);
    System.out.println(tokenId);
  }

  @AfterEach
  public void flushAfterEachTest() {
    entityManager.flush();
    entityManager.clear();
  }

  @Test
  public void testSuccessInIncreasingUserAndToken() {
    assertTrue(userRepository.count() > 0);
    User user = userRepository.findById(userId).orElseGet(null);
    assertNotNull(user);
    assertNotNull(user.getPassword());
    assertTrue(passwordEncoder.matches(password, user.getPassword()));
    assertTrue(tokenRepository.count() > 0);
    VerificationToken token = tokenRepository.findById(tokenId).orElseGet(null);
    assertNotNull(token);
    assertNotNull(token.getToken());
    assertEquals(token.getToken(), tokenChars);
  }


  @Test
  public void testSuccessInRemovingUserAndToken() {
    tokenRepository.deleteById(tokenId);
    userRepository.deleteById(userId);
  }
}