package com.example.canteen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.WebApplicationContext;
import com.example.canteen.dao.UserRepository;
import com.example.canteen.domain.User;
import com.example.canteen.dto.UserDto;
import com.example.canteen.helper.SecurityHelper;
import com.example.canteen.service.UserService;
import lombok.RequiredArgsConstructor;

@SpringBootTest
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceIntegrationTest {

  private final UserService userService;

  @BeforeEach
  public void setUp() {

  }

  @Test
  public void testTheTestAccountIsAvailable() {
    final String userEmail = "test@test.com";
    final User user = userService.findUserByEmail(userEmail);

    assertNotNull(user);
    assertNotNull(user.getEmail());
    assertEquals(userEmail, user.getEmail());
    assertNotNull(user.getId());
  }

  @Test
  public void testSuccessInRegisteringNewAccount() throws Exception {
    final String userEmail = UUID.randomUUID().toString();
    final UserDto userDto = createUserDto(userEmail);
    final User user = userService.registerNewUserAccount(userDto);

    assertNotNull(user);
    assertNotNull(user.getEmail());
    assertEquals(userEmail, user.getEmail());
    assertNotNull(user.getId());
  }

  private UserDto createUserDto(final String email) {
    final UserDto userDto = new UserDto();

    userDto.setEmail(email);
    userDto.setPassword("SecretPassword");
    userDto.setMatchingPassword("SecretPassword");
    userDto.setFirstName("First");
    userDto.setLastName("Last");
    userDto.setRole(0);

    return userDto;
  }
}
