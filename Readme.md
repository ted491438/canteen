## Getting Started
1. Open `src/main/resources/application.properties` to replace settings of email and database with yours.
2. You can access the application at:
[http://localhost:8080/](http://localhost:8080/)
3. Login into system by test user account `test@test.com`
4. You can fill out registration form to apply a new user account
5. Remember checking mailbox to verify your account

## Development Environment
+ MacOS Big Sur
+ JDK 11
+ Tomcat 9
+ Spring Boot 2.4.x
+ MySql 5.7

## Project structure
#### com.example.canteen.CanteenApplication
Bootstrap this Application
#### com.example.canteen.CanteenMvcConfig
Configuration for Spring MVC 
#### com.example.canteen.WebSeecurityConfig.
Configuration for Spring Security  
#### com.example.canteen.controller
Spring MVC controllers
#### com.example.canteen.dao
Spring Data Repositories
#### com.example.canteen.domain
Domain models
#### com.example.canteen.domain.pk
Composite Primary Keys
#### com.example.canteen.event
Application events
#### com.example.canteen.exception
User-defined exceptions
#### com.example.canteen.exception.handle
Exception handling
### com.example.canteen.handle
Handling any kind of events from spring framework
#### com.example.canteen.helper
Beans providing some functionalities,
#### com.example.canteen.listener
Application event listeners
#### com.example.canteen.service
MVC Server layer interface
#### com.example.canteen.service.impl
MVC Server layer implementation
#### com.example.canteen.util
Utility classes except beans
#### com.example.canteen.validation
User-defined validator

## E-R Diagram
![E-R Diagram](./ERDiagram.png)